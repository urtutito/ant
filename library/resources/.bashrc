# This file contains config for bash promt

        # Custom parameters
            colors="promt_color"
            clock="promt_clock"
            test="no"

        # Configs
            case $- in
                *i*) ;;
                *) return;;
            esac

        # History length and other history configs
            HISTCONTROL=ignoreboth
			shopt -s histappend
            HISTSIZE=1000
            HISTFILESIZE=2000

        # Check the window size after each command and, if necessary, update the values of LINES and COLUMNS.
            shopt -s checkwinsize

        # Other configs
            [ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
            if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
                debian_chroot=$(cat /etc/debian_chroot)
            fi
            case "$TERM" in
                xterm-color|*-256color) colors=yes;;
            esac
                
        # Color config
            # Enable color support of ls and also add handy aliases
                if [ -x /usr/bin/dircolors ]; then
                    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
                    alias ls="ls --color=auto"
                    #alias dir="dir --color=auto"
                    #alias vdir="vdir --color=auto"
                    alias grep="grep --color=auto"
                    alias fgrep="fgrep --color=auto"
                    alias egrep="egrep --color=auto"
                fi

        # Other configs
            [ -z "$PS1" ] && return

        # Bash promt
            if [ $test = yes ]; then
                PS1="& "
            fi
            if [ $colors = yes ] && [ $clock = yes ]; then
                PS1="\[\033[01;33m\][ \A ] \[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w \[\033[00m\]& "
            fi
            if [ $colors = no ] && [ $clock = yes ]; then
                PS1="[ \A ] \u@\h:\w & "
            fi
            if [ $colors = yes ] && [ $clock = no ]; then
                PS1="\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w \[\033[00m\]& "
            fi
            if [ $colors = no ] && [ $clock = no ]; then
                PS1="\u@\h:\w & "
            fi

        # Command alias
            # ls
                alias ll="ls -alF"
                alias la="ls -A"
                alias l="ls -CF"

            # nano
                alias nano="nano -l"
                alias nanosh="nano -Y sh"

            # Other commands
                alias publicip="dig +short myip.opendns.com @resolver1.opendns.com"
                alias manager="sh /ant/asys/manager.sh"
